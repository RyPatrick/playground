<?php
    // Display PHP errors
    ini_set('display_errors', 1);
    // 1. Create database connection
    require_once("../includes/db_connection.php");
    // Grab my PHP functions
    require_once('../includes/functions.php');
    
    // Pull all visible subjects from database
    $result = db_select("subjects", "visible", 1);

    // Determine which page to render
    if (isset($_GET['id'])) {
        $page = $_GET['id'];
    } else {
        $page = 1;
    }

    // Render page
    switch ($page) {
        case 1:
            render('templates/header', array(
                'title' => 'PHP Playground | Home',
                'keywords' => 'php, playground, home'
            ));
            render('home');
            render('templates/footer');
            break;
        
        case 2:
            render('templates/header', array(
                'title' => 'PHP Playground | Home',
                'keywords' => 'php, playground, home'
            ));
            render('images');
            render('templates/footer');
            break;
        
        case 3:
            render('templates/header', array(
                'title' => 'PHP Playground | Home',
                'keywords' => 'php, playground, home'
            ));
            render('about');
            render('templates/footer');
            break;
    }


    // 4. Release returned data
    mysqli_free_result($result);

    // End database connection
    if (isset($db)) {
        mysqli_close($db);
    }
?>