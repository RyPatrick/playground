<?php
    // 2. Perform database query
    $result  = db_select("pages", "subject_id", 3);

    // 3. Use returned data (if any)
    while($content = mysqli_fetch_assoc($result)) {
    // output data from each row
?>

    <div class="block-title">
        <h1><?php echo ucfirst($content['menu_name']); ?></h1>
    </div>
    <div class="center vertical-padding">
        <?php echo $content['content']; ?>
    </div>

<?php
    }

    // 4. Release returned data
    mysqli_free_result($result);
?>